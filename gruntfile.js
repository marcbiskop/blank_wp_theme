module.exports = function(grunt) {

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				options: {
					style: 'compressed', //nested, compact, compressed, expanded
					sourcemap: 'none',
					precision: 2 //decimal precision
				},
				files: {
					'css/main.min.css' : 'main.scss', //Destination : Source
					'css/main-admin.min.css' : 'scss/main-admin.scss'
				}
			}
		},

		//AUTOPREFIXER
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9']
			},
			dist: {
				files: {
					'css/main.min.css' : 'css/main.min.css'
				}
			}
		},

		//UGLIFY
		uglify: {
			build: {
				files: [{
					expand: true,
					src: 'js/*.js',
					dest: 'js/build/',
					flatten: true,
					rename: function(destBase, destPath){
						return destBase + destPath.replace(
							'.js', '.min.js');
					}
				}]
			}
		},

		//WATCH
		watch: {
			css: {
				files: ['main.scss', 'scss/*.scss'],
				tasks: ['sass', 'autoprefixer']
			},
			js: {
				files: ['js/*.js'],
				tasks: ['uglify']
			}
		}
	});

	grunt.registerTask('default', ['watch']);
}