<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #wrapper div element.
 *
 * @package Package
 * @subpackage THEME NAME
 * @since VERSIONING
 */
?>

		<?php
		/*if(is_active_sidebar('sidebar-footer')) {
			dynamic_sidebar('sidebar-footer');
		}*/
		?>

	</div> <!-- #wrapper -->
	<?php wp_footer(); ?>

</body>
</html>