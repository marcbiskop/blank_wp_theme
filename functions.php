<?php 

/**
 * Require custom post types
 */
include '/custom_post_types/enterprises.php';
//include '/custom_post_types/toplist.php';

/**
 * Require option page
 */
include '/option_page/theme_panel.php';

/**
 * Add support for thumbnails
 */
add_theme_support( 'post-thumbnails' ); 

/**
 * Enqueue scripts and styles
 */
function load_custom_wp_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/main-admin.min.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );

    wp_enqueue_script( 'admin-js', get_template_directory_uri() . '/js/admin.js', array(), '1.0', true );
    wp_enqueue_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCvhW3SYS7nfEQU8w-AX7tK6_2w2QL3o2w&libraries=places', false, '3');
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

function free_wifi_scripts() {
    wp_enqueue_style( 'foundationstyle', get_template_directory_uri() . '/vendor/foundation/css/foundation.min.css' );
    wp_enqueue_style( 'foundationflex', get_template_directory_uri() . '/vendor/foundation/css/foundation-flex.css' );
    wp_enqueue_style( 'main.min.css', get_template_directory_uri() . '/css/main.min.css' );

    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/vendor/foundation/js/vendor/jquery.min.js', array(), '1.12.0', true );
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/foundation/js/vendor/what-input.min.js', array(), '1.12.0', true );
    wp_enqueue_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), '2.8.3', true );
    wp_enqueue_script( 'foundation-script', get_template_directory_uri() . '/vendor/foundation/js/foundation.min.js', array(), '6.0', true );
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/build/main.min.js', array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'free_wifi_scripts' );

?>